#!/usr/bin/env iocsh.bash

# Required modules
## module_name,version

require(prempscananalog)

# -----------------------------------------------------------------------------
# IOC common settings
# -----------------------------------------------------------------------------
epicsEnvSet("ASYN_PORT",      "sad-plc-scan-001")
epicsEnvSet("IP_ADDR",        "172.30.244.59")
epicsEnvSet("PORT_ADDR",      "5000")
epicsEnvSet("PREFIX",         "SES-SCAN:LSR-001")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(prempscananalog_DIR)db/")
#iocshLoad("$(prempscananalog_DIR)prempscananalog.iocsh", "ASYN_PORT=$(ASYN_PORT), IP_ADDR=$(IP_ADDR), PORT_ADDR=$(PORT_ADDR), PREFIX=$(PREFIX)")
# -----------------------------------------------------------------------------
# IOC common settings
# -----------------------------------------------------------------------------
#epicsEnvSet("ASYN_PORT",      "$(ASYN_PORT)")
#epicsEnvSet("IP_ADDR",        "$(IP_ADDR)")
#epicsEnvSet("PORT_ADDR",      "$(PORT_ADDR)")
#epicsEnvSet("PREFIX",         "$(PREFIX)")

# Connection
drvAsynIPPortConfigure("$(ASYN_PORT)","$(IP_ADDR):$(PORT_ADDR)",0,0,0)

# Loading database
dbLoadRecords(prempScanAnalog.db, "P=$(PREFIX), PORT=$(ASYN_PORT), ADDR=01")


iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

# Start the IOC
iocInit()
